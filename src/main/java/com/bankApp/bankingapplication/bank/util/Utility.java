package com.bankApp.bankingapplication.bank.util;

import org.springframework.stereotype.Component;

@Component
public class Utility {
	public String generateNumber(int limit) {
		
		int[] NumArr = new int[10];

		for (int i = 0; i < 10; i++) {
			NumArr[i] = i;
		}

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < limit; i++) {
			sb.append(NumArr[rand()]);
		}
		String AccNo = sb.toString();
		return AccNo;
	}
	
	private int rand() {
		return (int) (Math.random() * 10);
	}

	public String generateName(String prefix, int suffixLimit) {
		String random = this.generateNumber(suffixLimit);

		return prefix + "-" + random;
	}

}
