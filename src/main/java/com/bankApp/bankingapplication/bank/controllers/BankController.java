package com.bankApp.bankingapplication.bank.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

//import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bankApp.bankingapplication.bank.beans.AccessGrant;
import com.bankApp.bankingapplication.bank.beans.AccountInfo;
import com.bankApp.bankingapplication.bank.beans.AccountStatement;
import com.bankApp.bankingapplication.bank.beans.Deposit;
import com.bankApp.bankingapplication.bank.beans.MsgBuilder;
import com.bankApp.bankingapplication.bank.beans.Withdrawal;
import com.bankApp.bankingapplication.bank.services.BankService;

@RestController
public class BankController {
	
	@Autowired
	private BankService bank;
	
	@RequestMapping("/account_info/{acctNum}")
	public MsgBuilder<AccountInfo> getInfo(@PathVariable("acctNum") String acctNum) {
		return bank.getAccountInfo(acctNum);
	}
	
	@RequestMapping("/account_statement/{acctNum}")
	public List<AccountStatement> getStatement(@PathVariable("acctNum") String acctNum) {
		return bank.getAccountStatement(acctNum);
	}
	
	@RequestMapping(value="/deposit", method=RequestMethod.POST)
	public MsgBuilder<Deposit> deposit(@RequestBody Deposit deposit) {
		return bank.makeDeposit(deposit);
	}
	
	@RequestMapping(value="/withdrawal", method=RequestMethod.POST)
	public MsgBuilder<Object> withdraw(@RequestBody Withdrawal obj) {
		return bank.withdrawal(obj);
	}
	
	public MsgBuilder<AccountInfo> createAcct(@RequestParam(value="acctName", defaultValue="") String name, @RequestParam(value="password", defaultValue="") String password, @RequestParam(defaultValue="0", value="deposit") Double deposit) {
		return bank.createAcct(name, password, deposit);
	}
	
	public AccessGrant login(@RequestParam(value="acctNum", defaultValue="") String number, @RequestParam(value="password", defaultValue="") String password) {
		return bank.login(number, password);
	}
}
