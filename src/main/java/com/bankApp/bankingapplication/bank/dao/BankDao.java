package com.bankApp.bankingapplication.bank.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bankApp.bankingapplication.bank.beans.Account;
import com.bankApp.bankingapplication.bank.beans.AccountInfo;
import com.bankApp.bankingapplication.bank.beans.AccountStatement;
import com.bankApp.bankingapplication.bank.beans.Deposit;
import com.bankApp.bankingapplication.bank.beans.Withdrawal;

@Component
public class BankDao {

	private List<AccountInfo> allAccounts = Arrays.asList(
			new AccountInfo("0123456789", 40000.00, "Davis_brown", "4321"),
			new AccountInfo("0133456789", 40000.00, "Chris_haris", "1234"));

	private List<Account> accounts = Arrays.asList(
			new Account("0123456789",
					Arrays.asList(new AccountStatement(new Date(), "deposit", null, 40000.00, 40000.00))),
			new Account("0133456789",
					Arrays.asList(new AccountStatement(new Date(), "deposit", null, 40000.00, 40000.00))));

	public AccountInfo getInfo(String acctNo) {
		return null;
	}

	public List<AccountStatement> getStatements(String acctNum) {
		// TODO Auto-generated method stub
		Account acct = new Account();
		for (Account account : accounts) {
			if (account.getAcctNum().equals(acctNum)) {
				acct = account;
			}
		}

		return acct.getStmt();
	}

	public boolean depositMoney(Deposit obj) {
		boolean done = false;

		for (AccountInfo info : allAccounts) {
			if (info.getAccountNumber().equals(obj.getAccountNumber())) {
				double balance = info.getAccountBalance();

				// Set the account statement
				for (Account account : accounts) {
					if (account.getAcctNum().equals(info.getAccountNumber())) {
						// set balance
						info.setAccountBalance(balance + obj.getAmount());
						// create and add statement
						AccountStatement stmt = new AccountStatement(new Date(), "deposit", null, obj.getAmount(),
								info.getAccountBalance());
						account.getStmt().add(stmt);

						done = true;
						break;
					}
				}
				break;
			}
		}
		return done;
	}

	public boolean withdraw(Withdrawal obj) {
		boolean done = false;

		for (AccountInfo info : allAccounts) {
			if (info.getAccountNumber().equals(obj.getAccountNumber())) {
				double balance = info.getAccountBalance();
				// validate password
				if (info.getAccountPassword().equals(obj.getAccountPassword())) {
					// Set the account statement
					for (Account account : accounts) {
						if (account.getAcctNum().equals(info.getAccountNumber())) {
							// set balance
							info.setAccountBalance(balance - obj.getWithdrawnAmmount());
							// create and add statement
							AccountStatement stmt = new AccountStatement(new Date(), "withdrawal", null,
									obj.getWithdrawnAmmount(), info.getAccountBalance());
							account.getStmt().add(stmt);

							done = true;
							break;
						}
					}
				}

				break;
			}
		}
		return done;
	}

	public boolean acctIsValid(String acctNo) {
		boolean valid = false;
		for (AccountInfo accountInfo : allAccounts) {
			if (accountInfo.getAccountNumber().equals(acctNo)) {
				valid = true;
				break;
			}
		}
		return valid;
	}

	public boolean nameIsValid(String name) {
		boolean valid = false;
		for (AccountInfo accountInfo : allAccounts) {
			if (accountInfo.getAccountName().equals(name)) {
				valid = true;
				break;
			}
		}
		return valid;
	}

	public boolean createAccount(AccountInfo info) {
		boolean done = true;
		
		allAccounts.add(info);
		return done;
	}
}
