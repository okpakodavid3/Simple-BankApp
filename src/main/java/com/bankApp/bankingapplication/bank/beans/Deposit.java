package com.bankApp.bankingapplication.bank.beans;

public class Deposit {
	private String accountNumber;
	private Double amount;
	public String message = "";

	public Deposit() {
	}

	public Deposit(String accountNumber, Double amount) {
		this.accountNumber = accountNumber;
		this.amount = amount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public boolean validate() {
		if (this.amount < 1000000 || this.amount > 1) {
			
			if(this.amount < 1000000) message = "Deposit cannot be greater than 1,000,000.00";
			else message = "Deposit cannot be less than 1.00";
			
			return false;
		}
			

		return true;
	}

}
