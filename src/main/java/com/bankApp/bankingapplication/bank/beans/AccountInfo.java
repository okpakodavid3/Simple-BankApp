package com.bankApp.bankingapplication.bank.beans;

public class AccountInfo {

	private String accountNumber;
	private double accountBalance;
	private String accountName;
	private String accountPassword;
	public AccountInfo() {
	}

	public AccountInfo(String accountNumber, double accountBalance, String accountName, String passcode) {
		super();
		this.accountNumber = accountNumber;
		this.accountBalance = accountBalance;
		this.accountName = accountName;
		this.accountPassword = passcode;
	}
	
	public String getAccountPassword() {
		return accountPassword;
	}

	public void setAccountPassword(String accountPassword) {
		this.accountPassword = accountPassword;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

}
