package com.bankApp.bankingapplication.bank.beans;

import java.util.List;

public class Account {
	private String acctNum;
	private List<AccountStatement> stmt;
	public String getAcctNum() {
		return acctNum;
	}
	public void setAcctNum(String acctNum) {
		this.acctNum = acctNum;
	}
	
	public List<AccountStatement> getStmt() {
		return stmt;
	}
	public void setStmt(List<AccountStatement> stmt) {
		this.stmt = stmt;
	}
	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Account(String acctNum, List<AccountStatement> stmt) {
		super();
		this.acctNum = acctNum;
		this.stmt = stmt;
	}
	
	
	
	
}
