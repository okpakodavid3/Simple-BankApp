package com.bankApp.bankingapplication.bank.beans;

public class MsgBuilder<T> {
	private int responseCode;
	private boolean success;
	private String message;
	private T account;
	
	public MsgBuilder() {
		this.responseCode = 200;
		this.success = true;
		this.message = "Loaded successfully";
	}
	
	public MsgBuilder(int responseCode, boolean success, String message, T object) {
		this.responseCode = responseCode;
		this.success = success;
		this.message = message;
		this.account = object;
	}

	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getAccount() {
		return account;
	}
	public void setAccount(T object) {
		this.account = object;
	}
}
