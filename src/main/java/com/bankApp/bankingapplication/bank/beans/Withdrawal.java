package com.bankApp.bankingapplication.bank.beans;

public class Withdrawal {
	private String accountNumber;
	private String accountPassword;
	private Double withdrawnAmmount;
	public String message = "";
	public Withdrawal() {
	}

	public Withdrawal(String accountNumber, String accountPassword, Double withdrawnAmmount) {
		this.accountNumber = accountNumber;
		this.accountPassword = accountPassword;
		this.withdrawnAmmount = withdrawnAmmount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountPassword() {
		return accountPassword;
	}

	public void setAccountPassword(String accountPassword) {
		this.accountPassword = accountPassword;
	}

	public Double getWithdrawnAmmount() {
		return withdrawnAmmount;
	}

	public void setWithdrawnAmmount(Double withdrawnAmmount) {
		this.withdrawnAmmount = withdrawnAmmount;
	}

	public boolean validate() {
		if(this.withdrawnAmmount < 1.00) {
			message = "Withdrawn amount cannot be less than 1.00";
			return false;
		}
		return true;
	}
}
