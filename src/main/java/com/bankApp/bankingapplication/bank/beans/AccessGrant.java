package com.bankApp.bankingapplication.bank.beans;

public class AccessGrant {
	private boolean success;
	private String accessToken;
	private String message;
	private int responseCode;
	
	
	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public AccessGrant(boolean success, String accessToken) {
		super();
		this.success = success;
		this.accessToken = accessToken;
	}

	public AccessGrant() {
	}

	public AccessGrant(boolean success, String accessToken, String message, int responseCode) {
		super();
		this.success = success;
		this.accessToken = accessToken;
		this.message = message;
		this.responseCode = responseCode;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

}
