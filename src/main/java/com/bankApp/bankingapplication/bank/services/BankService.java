package com.bankApp.bankingapplication.bank.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankApp.bankingapplication.bank.beans.AccessGrant;
import com.bankApp.bankingapplication.bank.beans.AccountInfo;
import com.bankApp.bankingapplication.bank.beans.AccountStatement;
import com.bankApp.bankingapplication.bank.beans.Deposit;
import com.bankApp.bankingapplication.bank.beans.MsgBuilder;
import com.bankApp.bankingapplication.bank.beans.Withdrawal;
import com.bankApp.bankingapplication.bank.dao.BankDao;
import com.bankApp.bankingapplication.bank.util.Utility;

@Service
public class BankService {

	@Autowired
	private BankDao bank;
	@Autowired
	private Utility utils;

	public MsgBuilder<AccountInfo> getAccountInfo(String acctNo) {
//		 System.out.println(utils.generateNumber(6));
		AccountInfo accountInfo = bank.getInfo(acctNo);
		MsgBuilder<AccountInfo> msg = new MsgBuilder<>();

		if (accountInfo == null) {
			msg.setResponseCode(400);
			msg.setMessage("Account not found!!...");
			msg.setSuccess(false);
		}

		msg.setAccount(accountInfo);
		return msg;
	}

	public List<AccountStatement> getAccountStatement(String acctNum) {
		List<AccountStatement> stmts = bank.getStatements(acctNum);
		return stmts;
	}

	public MsgBuilder<Deposit> makeDeposit(Deposit obj) {
		// TODO Auto-generated method stub
		MsgBuilder<Deposit> msg = new MsgBuilder<Deposit>();
		boolean isValid = obj.validate();

		if (isValid) {
			if (bank.acctIsValid(obj.getAccountNumber())) {
				boolean sent = bank.depositMoney(obj);
				msg.setAccount(obj);
				if (sent) {
					msg.setMessage("Deposited successfully!!");
				} else {
					msg.setMessage("An error occured.. Try again");
					msg.setResponseCode(400);
				}
			} else {
				msg.setMessage("An error occured.. Account Number doesn't exist!!");
				msg.setResponseCode(400);
			}
		} else {
			msg.setMessage(obj.message);
			msg.setSuccess(false);
			msg.setResponseCode(400);
		}

		return msg;
	}

	public MsgBuilder<Object> withdrawal(Withdrawal obj) {
		MsgBuilder<Object> msg = new MsgBuilder<Object>();
		msg.setAccount(obj);

		if (bank.acctIsValid(obj.getAccountNumber())) {
			if (obj.validate()) {
				boolean done = bank.withdraw(obj);

				if (done) {
					msg.setMessage("Money withdrawn successfully");
				}
			} else {
				msg.setMessage(obj.message);
				msg.setResponseCode(400);
			}

		} else {
			msg.setMessage("Account is invalid");
			msg.setResponseCode(400);
		}

		return msg;
	}

	public MsgBuilder<AccountInfo> createAcct(String name, String password, Double deposit) {
		MsgBuilder<AccountInfo> msg = new MsgBuilder<AccountInfo>();
		AccountInfo info = new AccountInfo();
		msg.setSuccess(false);

		boolean error = false;

		// Validate entries
		if (password.isEmpty())
			error = true;
		if (name.isEmpty())
			error = true;
		if (deposit < 500)
			error = true;
		/// ---------------

		if (error) {
			msg.setResponseCode(400);
			msg.setMessage("An error occured.. Try again");
		} else {
			if (!bank.nameIsValid(name)) {
				String acctNo = utils.generateNumber(10);
				// Make sure number doesn't exist
				while (bank.acctIsValid(acctNo))
					acctNo = utils.generateNumber(10);

				info.setAccountName(name);
				info.setAccountPassword(password);
				info.setAccountBalance(deposit);
				info.setAccountNumber(acctNo);

				if (bank.createAccount(info)) {
					msg.setAccount(info);
					msg.setMessage("Account created successfully");
					msg.setSuccess(true);
				} else {
					msg.setMessage("An error occured. Try again");
					msg.setResponseCode(400);
				}
			} else {
				String randomName = utils.generateName(name, 4);
				// Make sure name doesn't exist
				while (bank.nameIsValid(randomName))
					randomName = utils.generateName(name, 4);

				msg.setMessage("Oops!! Account with name already exists. Try this: " + randomName);
				msg.setResponseCode(400);
			}
		}

		return msg;
	}

	public AccessGrant login(String number, String password) {
		AccessGrant access = new AccessGrant(false, null, "Access denied!! Invalid credentials", 500);

		boolean error = false;

		if (number.isEmpty())
			error = true;
		if (password.isEmpty())
			error = true;

		if (!error) {
			if (bank.acctIsValid(number)) {
				AccountInfo info = bank.getInfo(number);
				if (info.getAccountPassword().equals(password)) {
					access.setAccessToken(utils.generateNumber(6));
					access.setMessage("Logged in Successfully");
				}
			}
		}

		return access;
	}
}
